#include "LZespolona.hh"
#include <cmath>
#include <iostream>
/*!
 * Realizuje dodanie dwoch liczb zespolonych.
 * Argumenty:
 *    Skl1 - pierwszy skladnik dodawania,
 *    Skl2 - drugi skladnik dodawania.
 * Zwraca:
 *    Sume dwoch skladnikow przekazanych jako parametry.
 */
ostream& operator << (ostream& StrWyj, LZespolona liczba)
{
  StrWyj << "(" << liczba.re << showpos << liczba.im << "i)" << noshowpos;
  return StrWyj;
}

istream& operator >> (istream& StrWej, LZespolona& liczba)
{
	char znak;

	StrWej >> znak;
	if  (znak != '(')
	{
StrWej.setstate(ios::failbit);
		return StrWej;
	}

	StrWej >> liczba.re;
	if (!StrWej)
	{
		StrWej.setstate(ios::failbit);
		return StrWej;

	}

	StrWej >> liczba.im;
	if (!StrWej)
	{
		StrWej.setstate(ios::failbit);
		return StrWej;
	}

	StrWej >> znak;
	if  (znak != 'i')
	{
		StrWej.setstate(ios::failbit);
		return StrWej;
	}

	StrWej >> znak;
	if  (znak != ')')
	{
		StrWej.setstate(ios::failbit);
		return StrWej;
	}

	return StrWej;
}

LZespolona sprzez (LZespolona Skl)
{
    Skl.im=-(Skl.im);
    return Skl;
}
double modul (LZespolona Skl)
{
    double Wynik;
    Wynik = sqrt(pow(Skl.re, 2) + pow(Skl.im, 2));
    return Wynik;
}
LZespolona operator / (LZespolona Skl1, LZespolona Skl2)
{
    LZespolona Wynik;

    Wynik.re = ((Skl1.re)*(Skl2.re)+(Skl1.im)*(Skl2.im))/pow(modul(Skl2), 2);
    Wynik.im = ((Skl1.im)*(Skl2.re)-(Skl1.re)*(Skl2.im))/pow(modul(Skl2), 2);
    return Wynik;
}
LZespolona operator * (LZespolona Skl1, LZespolona Skl2)
{    LZespolona Wynik;

    Wynik.re = ((Skl1.re)*(Skl2.re)-(Skl1.im)*(Skl2.im));
    Wynik.im = ((Skl1.re)*(Skl2.im)+(Skl2.re)*(Skl1.im));
    return Wynik;
}
LZespolona  operator + (LZespolona  Skl1,  LZespolona  Skl2)
{
  LZespolona  Wynik;

  Wynik.re = Skl1.re + Skl2.re;
  Wynik.im = Skl1.im + Skl2.im;
  return Wynik;
}
LZespolona operator - (LZespolona  Skl1,  LZespolona  Skl2)
{
  LZespolona  Wynik;

  Wynik.re = Skl1.re - Skl2.re;
  Wynik.im = Skl1.im - Skl2.im;
  return Wynik;
}
void wyswietl(LZespolona arg)
{
    cout<<'('<<arg.re;
    if(arg.im>0)
    {
        cout<<'+';
    }
    cout<<arg.im<<"i)"<<endl;
}

/*LZespolona operator % (LZespolona Skl1, LZespolona Skl2)
{
LZespolona Wynik;

if((Skl2.re<Skl1.re)&&(Skl2.im<Skl1.im))
{
  Wynik.re=0;
  Wynik.im=0;
  return Wynik;
}
if((Skl2.re<Skl1.re)&&(Skl2.im>=Skl1.im))
{
  Wynik.re=0;
  Wynik.im=0;
  return Wynik;
}
if((Skl2.re>=Skl1.re)&&(Skl2.im<Skl1.im))
{
  Wynik.re=0;
  Wynik.im=0;
  return Wynik;
}
if((Skl2.re>=Skl1.re)&&(Skl2.im>=Skl1.im))
{
Wynik.re=(Skl1.re)%(Skl2.re);
Wynik.im=(Skl1.im)%(Skl2.im);
}
}
*/
LZespolona Wczytaj()
{
  LZespolona liczba;
  char znak;

  cin >> znak;
  cin >> liczba.re;
  cin >> znak;
  cin >> liczba.im;
  if(znak=='-')
  {
  	liczba.im=liczba.im*(-1);
  }
  cin >> znak;
  cin >> znak;
  return liczba;
}
