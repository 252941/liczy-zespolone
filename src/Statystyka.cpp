#include "Statystyka.hh"
#include <iostream>
#include <cmath>

void WyswietlWynik(statystyka s)
{
    double Wynik;
    Wynik = (double(s.dobrze) / (double(s.dobrze) + double(s.zle))) * 100;
    cout << "Liczba dobrych odpowiedzi: " << s.dobrze << endl;
    cout << "Liczba zlych odpowiedzi: " << s.zle << endl;
    cout << "Wynik procentowy poprawnych odpowiedzi: " << Wynik << "%" << endl;
}