#include <iostream>
#include "BazaTestu.hh"
#include "Statystyka.hh"
using namespace std;

int main(int argc, char **argv)
{

  if (argc < 2) {
    cout << endl;
    cout << " Brak opcji okreslajacej rodzaj testu." << endl;
    cout << " Dopuszczalne nazwy to:  latwy, trudny." << endl;
    cout << endl;
    return 1;
  }

  BazaTestu   BazaT = { nullptr, 0, 0 };

  if (InicjalizujTest(&BazaT,argv[1]) == false) {
    cerr << " Inicjalizacja testu nie powiodla sie." << endl;
    return 1;
  }

  cout << endl;
  cout << " Start testu arytmetyki zespolonej: " << argv[1] << endl;
  cout << endl;

  WyrazenieZesp   WyrZ_PytanieTestowe;
  LZespolona liczba1, liczba2;
  statystyka s;
  double p,q;
  s.dobrze = 0;
  s.zle = 0;
  
  while (PobierzNastpnePytanie(&BazaT,&WyrZ_PytanieTestowe)) {
    cout << "Podaj wynik: ";
    cout << WyrZ_PytanieTestowe << endl;
    cout << "Twoja odpowiedz to: ";
    cin >> liczba1;
    while(cin.fail())
    {
cin.clear();
cin.ignore(10000, '\n');
cerr << "Zle zapisana liczba zespolona";
cout << endl;
cin >> liczba1;
    }
    p = liczba1.re; // wprowadzenie czesci rzeczywistej i urojonej liczby od uzytkownika pod zmienne pomocnicze w celu dalszego porownania
    q = liczba1.im;
    liczba2=Oblicz (WyrZ_PytanieTestowe); // obliczanie prawdilowego wyniku
    if(liczba2.re == p && liczba2.im == q) // jesli porownanie liczb sie zgadza to odpowiedz dobra
    {
    	cout << "Dobry wynik" << endl;
    	++s.dobrze; // zliczanie dobrych odpowiedzi
	}
	else // gdy porownanie zle, odpowiedz niepoprawna
	{
		cout << "Zly wynik" << endl << "Poprawnym wynikiem jest: " << liczba2 << endl << endl;
		++s.zle; // zliczanie zlych odpowiedzi
	}
  }
  cout << endl;
  cout << " Koniec testu" << endl;
  cout << endl;
  WyswietlWynik(s); // wyswietlanie wyniku
}
