#include "WyrazenieZesp.hh"
using namespace std;

/*
 * Tu nalezy zdefiniowac funkcje, ktorych zapowiedzi znajduja sie
 * w pliku naglowkowym.
 */
/*void WyswietlWyrazenie (WyrazenieZesp WyrZ)
{
  Wyswietl (WyrZ.Arg1);
  WyswietlSymbol (WyrZ.Op);
  Wyswietl (WyrZ.Arg2);
  cout << endl;
}
*/
LZespolona Oblicz (WyrazenieZesp WyrZ)
{
  LZespolona B;
  switch (WyrZ.Op){
  case Op_Dodaj:
    return operator + (WyrZ.Arg1, WyrZ.Arg2);
    break;

  case Op_Odejmij:
    return operator - (WyrZ.Arg1, WyrZ.Arg2);
    break;

  case Op_Mnoz:
    return operator * (WyrZ.Arg1, WyrZ.Arg2);
    break;

  case Op_Dziel:
    return operator / (WyrZ.Arg1, WyrZ.Arg2);
    break;
  }
  return B;
}
char Symbol (Operator oper)
{
  char O;
  switch (oper){
  case Op_Dodaj:
    O = '+';
    break;

  case Op_Odejmij:
    O = '-';
    break;

  case Op_Mnoz:
    O = '*';
    break;

  case Op_Dziel:
    O = '/';
    break;
      }
  return O;
}
ostream& operator << (ostream& StrWyj, WyrazenieZesp Wyr)
{
  StrWyj << Wyr.Arg1;
  StrWyj << Symbol (Wyr.Op);
  StrWyj << Wyr.Arg2 << endl;
  return StrWyj;
}
istream& operator >> (istream& StrWej, WyrazenieZesp Wyr)
{
	char znak;
	StrWej >> Wyr.Arg1;
	StrWej >> znak;
	switch(znak)
    {
       case '+':
    Wyr.Op = Op_Dodaj;
    break;

       case '-':
    Wyr.Op = Op_Odejmij;
    break;

        case '*':
    Wyr.Op = Op_Mnoz;
    break;

         case '/':
    Wyr.Op = Op_Dziel;
    break;
  }
    StrWej >> Wyr.Arg2;
	return StrWej;
    }
