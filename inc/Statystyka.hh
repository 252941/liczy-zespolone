#ifndef STATYSTYKA_HH
#define STATYSTYKA_HH
#include <iostream>

using namespace std;


 /*********************************************/
 /* Modeluje pojecie dobrej i zlej odpowiedzi */
 /*********************************************/

struct statystyka {

  int dobrze;
  int zle;
};


 /*********************************************/
 /* Zapowiedz funkcji wyswietlania statystyki */
 /*********************************************/

void WyswietlWynik(statystyka s);

#endif
