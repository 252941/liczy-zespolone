#ifndef WYRAZENIEZESP_HH
#define WYRAZENIEZESP_HH
#include "LZespolona.hh"
using namespace std;

/*!
 * Modeluje zbior operatorow arytmetycznych.
 */
enum Operator { Op_Dodaj, Op_Odejmij, Op_Mnoz, Op_Dziel };



/*
 * Modeluje pojecie dwuargumentowego wyrazenia zespolonego
 */
struct WyrazenieZesp {
  LZespolona   Arg1;   // Pierwszy argument wyrazenia arytmetycznego
  Operator     Op;     // Opertor wyrazenia arytmetycznego
  LZespolona   Arg2;   // Drugi argument wyrazenia arytmetycznego
};


/*
 * Funkcje ponizej nalezy zdefiniowac w module.
 *
 */


void WyswietlWyrazenie (WyrazenieZesp WyrZ);
LZespolona Oblicz(WyrazenieZesp  WyrZ);
char Symbol (Operator oper);
ostream& operator << (ostream& StrWyj, WyrazenieZesp Wyr);
istream& operator >> (istream& StrWej, WyrazenieZesp Wyr);
#endif
